#OBS! Siden er ikke live!
***

#Find My Things (Working title)

######"Find My Thing is a service that let's you employ a personal searcher for a cheap cost.",

A single page site that at the current state is nothing but a form. Facebook integration will follow.

##Technology

* ###[Node.js](https://nodejs.org/)
     
    Used as the runtime envoirement for our JavaScript codebase.   
***

* ###[Express.js](http://expressjs.com/)

    Express is the framework used to easily and painlessly serve the handlebar templates via it's routing.
***

* ###[Nodemailer](http://www.nodemailer.com/)

    Transport module hooking up to gmail's SMTP protocol and slings forth the mail to the designated address.
***

* ###[Handlebars.js](http://handlebarsjs.com/)

    Very simple and highly itterated templating engine. Credit has to be given to this specific module creator [ericf](https://github.com/ericf/express-handlebars). 
***

* ###[Materialize](http://materializecss.com/)

    Front-end framework with it's roots in the material design language of Android.
***

* ###[Slick.js](http://kenwheeler.github.io/slick/)

    Used for the home pages carousel.
***