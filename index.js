var express = require('express');
var exphbs  = require('express-handlebars');
var bodyParser = require('body-parser');
var nodemailer = require('nodemailer');
var couch = require('./database/setup.js');

var app = express();

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



//Routes
app.get('/', function (req, res) {
    res.render('home');
});

app.post('/done', function (req, res) {

    couch.requests.insert({

        email: req.body.email,
        titel: req.body.titel,
        description: req.body.description,
        link: req.body.link,
        name: req.body.first_name + " " + req.body.last_name,
        address: req.body.address,
        tel: req.body.tel,
        ip: req.connection.remoteAddress },

        function(err, body) {

            if (!err)
            console.log(body);
            res.render('done', { title: 'FindMyThings', msg: 'Data has been saved!', err: true, page: 'done' });

        });

        /*
        var mailOpts, smtpTrans;

        smtpTrans = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
        user: 'add@mail.com',
        pass: 'password'
    }
});

mailOpts = {
from: req.body.email,
to: 'receiver@mail.com',
subject: req.body.titel,
text: req.body.description + " " + req.body.link
};

smtpTrans.sendMail(mailOpts, function (error, info) {
//Email not sent
if (error) {
res.render('done', { title: 'FindMyThings - Error', msg: 'Error. We could not handle your request.', err: true, page: 'done' })
console.log("Message not sent!");
}
//Email sent
else {
res.render('done', { title: 'FindMyThings - Mail Sent!', msg: 'Mail sent! You will hear from us as soon as possible.', err: false, page: 'done' })
console.log("Message sent!");
}
});

*/

});


//Start
app.listen(3000, function() {
    console.log("Listening on port 3000");
});
