$(document).ready(function(){
  $('.slider').slick({
      arrows: false,
      infinite: false,
      adaptiveHeight: true,
      swipe: false
  });
});

$('.btn-next').click(function(){
    $("#slider").slick('slickNext');
});

$('.btn-prev').click(function(){
    $("#slider").slick('slickPrev');
});
